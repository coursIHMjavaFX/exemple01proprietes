package edu.mermet;


public class App {
    public static void main(String[] args) {
	Personne p1 = new Personne("Martin", 12);
	p1.nomProperty().addListener(
			(prop, ancien, nouveau) -> System.out.println("Le nom de p1 est maintenant " + nouveau));
	Personne p2 = new Personne("Durand", 20);
	System.out.println("----");
	System.out.println("p1 = " + p1);
	System.out.println("p2 = " + p2);
	System.out.println("----");
	//p1.setNom("Dupont");
	p1.nomProperty().setValue("Dupont");
	// la ligne suivante n'est pas valide
	//p1.prenomProperty().setValue("Louis");
	p1.setPrenom("Louis");
	System.out.println("p1 = " + p1);
	System.out.println("p2 = " + p2);
	System.out.println("----");
	p2.nomProperty().bind(p1.nomProperty());
	p1.setNom("Macron");
	System.out.println("p1 = " + p1);
	System.out.println("p2 = " + p2);
	//La ligne suivante lève une exception
	//p2.setNom("Castex");
	System.out.println("----");
	p2.ageProperty().bindBidirectional(p1.ageProperty());
	p1.setAge(45);
	System.out.println("p1 = " + p1);
	System.out.println("p2 = " + p2);
	System.out.println("----");
	p2.setAge(60);
	System.out.println("p1 = " + p1);
	System.out.println("p2 = " + p2);
    }
}

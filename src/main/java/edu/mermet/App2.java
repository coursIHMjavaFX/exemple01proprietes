package edu.mermet;

public class App2 {
	public static void main(String[] args) {
		Monde leMonde = new Monde();
		Personne p1 = new Personne("Martin", 12);
		Personne p2 = new Personne("Durand", 20);
		leMonde.populationProperty().addAll(p1, p2);
		leMonde.populationProperty().remove(p2);
		leMonde.populationProperty().add(p2);
		leMonde.populationProperty().remove(p1);
		
	}
}
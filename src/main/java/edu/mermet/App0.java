package edu.mermet;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.StringProperty;

public class App0 {
	public static void main(String[] args) {
		Personne p1 = new Personne("Martin", 12);
		StringProperty nom = p1.nomProperty();
		System.out.println(nom.getValue());
		p1.nomProperty().addListener(
				(prop, ancien, nouveau) -> System.out.println("Le nom de p1 est maintenant " + nouveau));
		p1.setNom("Dupont");
		p1.nomProperty().setValue("Durand");
		p1.prenomProperty().addListener(
				(prop, ancien, nouveau) -> System.out.println("Le prénom de p1 est maintenant " + nouveau));
		p1.setPrenom("Marcel");
		ReadOnlyStringProperty prenom = p1.prenomProperty();
		System.out.println(prenom.getValue());
		//p1.prenomProperty().setValue("Jules");
	}
}
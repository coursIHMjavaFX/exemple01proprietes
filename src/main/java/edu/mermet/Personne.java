package edu.mermet;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

class Personne {
    private StringProperty nom;
    private ReadOnlyStringWrapper prenom;
    private IntegerProperty age;

    public Personne(String leNom, int lAge) {
	nom = new SimpleStringProperty(this, "nom", leNom);
	prenom = new ReadOnlyStringWrapper(this, "prenom", "Léonard");
	age = new SimpleIntegerProperty(this, "age", lAge);
    }


    public StringProperty nomProperty() {
	return nom;
    }

    @Override
    public String toString() {
	return prenom.getValue() + " " +
			nom.getValue() +
			"(" + age.getValue() + ")";
    }

    public IntegerProperty ageProperty() {
	return age;
    }
  
    public ReadOnlyStringProperty prenomProperty() {
    	return prenom.getReadOnlyProperty();
    }

    public void setNom(String leNom) {
    	if (leNom != null && !("".equals(leNom))) {
    		nom.setValue(leNom.toUpperCase());
    	}
    }

    public void setPrenom(String lePrenom) {
    	if (lePrenom != null && !("".equals(lePrenom))) {
    		prenom.setValue(lePrenom.toUpperCase());
    	}
    }
    
    public void setAge(int lAge) {
	age.setValue(lAge);
    }
}

package edu.mermet;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class Monde {
	private ObservableList<Personne> population;
	
	public Monde() {
		population = FXCollections.observableArrayList();
		population.addListener(new EnregistrementNaissance());
		population.addListener(new EnregistrementDeces());		
	}
	
	public ObservableList<Personne> populationProperty() {
		return population;
	}
	
	class EnregistrementNaissance implements ListChangeListener<Personne> {
		@Override
		public void onChanged(Change<? extends Personne> c) {
			boolean naissance = false;
			while (c.next()) {
				if (c.wasAdded()) {
					naissance = true;
					break;
				}
			}
			if (naissance) {
				System.out.println("Naissance");
				c.getAddedSubList().forEach(p -> System.out.println("  " + p));
			}			
		}
	}

	class EnregistrementDeces implements ListChangeListener<Personne> {
		@Override
		public void onChanged(Change<? extends Personne> c) {
			boolean deces = false;
			while (c.next()) {
				if (c.wasRemoved()) {
					deces = true;
					break;
				}
			}
			if (deces) {
				System.out.println("Décès");
				c.getRemoved().forEach(p -> System.out.println("  " + p));
			}			
		}
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		for (Personne p: population) {
			res.append(p.toString());
			res.append("\n");
		}
		return res.toString();
	}
}
